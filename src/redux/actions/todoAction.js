import axios from 'axios';

import { GET_ERRORS, POST_TODOS,GET_TODOS, PUT_TODOS,DELETE_TODOS } from './types';


export const toDoList = () => async (dispatch) => {
	axios
		.get(process.env.REACT_APP_API_URL + 'todos')
		.then((res) => {
			dispatch({
				type: GET_TODOS,
				payload: res.data
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
		});
};


export const toDoCreate = (todoData) => async (dispatch) => {
	axios
		.post(process.env.REACT_APP_API_URL + 'todos',todoData)
		.then((res) => {
			dispatch({
				type: POST_TODOS,
				payload: res.data
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
		});
};

export const toDoUpdate = (id,todoData) => async (dispatch) => {
	delete todoData.id;
	axios
		.put(process.env.REACT_APP_API_URL + 'todos/' + id,todoData)
		.then((res) => {
			dispatch({
				type: PUT_TODOS,
				payload: res.data
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
		});
};

export const toDoDelete = (id) => async (dispatch) => {
	axios
		.delete(process.env.REACT_APP_API_URL + 'todos/' + id)
		.then((res) => {
			dispatch({
				type: DELETE_TODOS,
				payload: res.data
			});
		})
		.catch((err) => {
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			});
		});
};
