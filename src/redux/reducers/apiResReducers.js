import { act } from 'react-dom/test-utils';
import { GET_TODOS, GET_ERRORS,POST_TODOS,PUT_TODOS,DELETE_TODOS } from '../actions/types';
const initialState = {};

export default function apiResReducer(state = initialState, action) {
	switch (action.type) {
		case GET_TODOS:
			return {
				list: action.payload,
				type:"GET_TODOS"
			}
		case POST_TODOS:
			return {
				create: action.payload,
				type:"POST_TODOS"
			}
		case PUT_TODOS:
			return {
				update: action.payload,
				type:"PUT_TODOS"
			}
		case DELETE_TODOS:
			return {
				delete: action.payload,
				type:"DELETE_TODOS"
			}
		case GET_ERRORS:
			return {
				error: action.payload,
				type:"GET_ERRORS"
			}
		default:
			return state;
	}
}
