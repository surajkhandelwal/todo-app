import { combineReducers } from 'redux';
import apiResReducer from './apiResReducers';

export default combineReducers({
	apiRes: apiResReducer
});
