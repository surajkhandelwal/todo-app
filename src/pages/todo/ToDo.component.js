import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { useForm, Controller } from 'react-hook-form';

import { ToDoList } from './components/ToDoList.component';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export function ToDoComponent({ toDoListData, toDoCreate, toDoUpdate, toDoDelete }) {
	const [selectedTodo, setSelectedToDo] = useState(null);
	const [list, setList] = useState([]);
	const { register, control, handleSubmit, watch, reset, setValue, errors } = useForm();

	const onSubmit = (data, e) => {
		console.log(data);
		if (!!data.id) {
			toDoUpdate(data.id, data);
			setValue('id', '');
			setValue('title', '');
			setValue('description', '');
			toast.success('ToDo updated successfully.');
		} else {
			delete data.id;
			toDoCreate(data);
			e.target.reset();
			toast.success('ToDo added successfully.');
		}
	};

	useEffect(() => {
		setList(toDoListData);
	}, [toDoListData]);

	return (
		<div>
			{/* navbar start  */}
			<div className="navbar navbar-expand flex-column flex-md-row align-items-center">
				<div className="container-fluid border-top border-bottom">
					<Link to="/" className="navbar-brand mr-0 mr-md-2 logo text-dark ml-3">
						TODO App
					</Link>
					<ul className="navbar-nav flex-row ml-auto d-flex align-items-center list-unstyled mb-0">
						<li className="nav-item border p-1 bg-light mr-3">ToDo List</li>
					</ul>
				</div>
			</div>
			{/* navbar */}

			{/* main content start */}

			<div className="container-fluid mt-2">
				<div className="container">
					{/* todo form start */}
					<div className="card border-0">
						<div className="card-header p-2">
							<h6 className="ml-3">Note Details</h6>
						</div>
						<div className="card-body mt-3  border-bottom">
							<form onSubmit={handleSubmit(onSubmit)}>
								<Controller
									render={({ field }) => <input type="hidden" value={selectedTodo?.id ?? ''} />}
									control={control}
									name="id"
									type={'hidden'}
									defaultValue={selectedTodo?.id ?? ''}
								/>

								<div className="form-group">
									<label htmlFor="title">Title</label>
									<input
										className="form-control"
										type="text"
										placeholder="Title"
										{...register('title', { required: true })}
										defaultValue={selectedTodo?.title ?? ''}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="description">Description</label>
									<input
										className="form-control"
										id="description"
										type="text"
										placeholder="Enter Description"
										{...register('description', { required: true })}
										defaultValue={selectedTodo?.description ?? ''}
									/>
								</div>
								<button type="submit" className="btn btn-primary">
									Submit
								</button>
							</form>
						</div>
					</div>
					{/* todo form end */}
					<ToDoList {...{ setValue, list, toDoDelete }} />
				</div>
			</div>

			{/* main content */}
			<ToastContainer
				position="top-center"
				autoClose={3000}
				hideProgressBar={true}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/>
		</div>
	);
}
