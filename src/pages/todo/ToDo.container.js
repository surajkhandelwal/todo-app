import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { ToDoComponent } from './ToDo.component';
import { toDoList, toDoCreate, toDoUpdate, toDoDelete } from '../../redux/actions/todoAction';

const ToDoContainer = props => {
	const [toDoListData, setToDoListData] = useState(null);
	useEffect(() => {
		props.toDoList();
	}, []);

	useEffect(() => {
		setToDoListData(props.listData);
	}, [props.listData]);

	useEffect(() => {
		props.toDoList();
	}, [props.createData]);

	useEffect(() => {
		props.toDoList();
	}, [props.deleteData]);

	useEffect(() => {
		props.toDoList();
	}, [props.updateData]);

	return <ToDoComponent toDoListData={toDoListData} {...props} />;
};

const mapStateToProps = state => ({
	listData: state.apiRes?.list,
	createData: state.apiRes?.create,
	updateData: state.apiRes?.update,
	deleteData: state.apiRes?.delete,
});

const ToDo = connect(mapStateToProps, {
	toDoList,
	toDoCreate,
	toDoUpdate,
	toDoDelete
})(ToDoContainer);
export default ToDo;
