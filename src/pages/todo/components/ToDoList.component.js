import { toast } from 'react-toastify';

export function ToDoList({ list, setValue, toDoDelete }) {
	return(
		<div className="card border-0 mt-3">
						<div className="card-header p-2">
							<h6 className="ml-3">TODO List</h6>
						</div>

						<div className="card-body mt-3  border-bottom">
							<div className="row justify-content-center">
								{list && list.length
									? list.map((item, i) => {
											return (
												<div className="card col-lg-3 m-1" key={i}>
													<div className="card-body">
														<h5 className="card-title">{item.title}</h5>
														<p className="card-text">{item.description}</p>
														<div className="justify-content-around row">
															<button
																className="btn btn-info"
																onClick={e => {
																	setValue('id', item.id);
																	setValue('title', item.title);
																	setValue('description', item.description);
																}}
															>
																Edit
															</button>
															<button
																className="btn btn-danger"
																onClick={e => {
																		toDoDelete(item.id);
																		toast.success('ToDo removed successfully.');
																}}
															>
																Delete
															</button>
														</div>
													</div>
												</div>
											);
									  })
									: 'No Data Found!'}
							</div>
						</div>
					</div>
	)
}