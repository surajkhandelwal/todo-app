import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ToDo from './pages/todo';

// const ToDo = React.lazy(() => import('./views/todo/ToDo'));

const loading = (
	<div className="pt-3 text-center">
		<div className="sk-spinner sk-spinner-pulse"></div>
	</div>
);

function App() {
	return (
		<BrowserRouter>
			<React.Suspense fallback={loading}>
				<Switch>
					<Route exact path="/" name="todo-list" render={props => <ToDo {...props} />} />
					<Route exact path="/todo-list" name="todo-list" render={props => <ToDo {...props} />} />
				</Switch>
			</React.Suspense>
		</BrowserRouter>
	);
}

export default App;
